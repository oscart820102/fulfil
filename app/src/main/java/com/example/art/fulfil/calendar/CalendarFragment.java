package com.example.art.fulfil.calendar;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.Constant;
import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.FragmentCalendarBinding;
import com.example.art.fulfil.manager.RealmManager;
import com.example.art.fulfil.model.PlanModel;
import com.example.art.fulfil.model.RecordModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.List;


public class CalendarFragment extends Fragment {
    private FragmentCalendarBinding binding;
    private List<CalendarDay> plans = new ArrayList<>();
    private List<CalendarDay> records = new ArrayList<>();


    public CalendarFragment() {
        // Required empty public constructor
    }


    public static CalendarFragment newInstance() {
        CalendarFragment fragment = new CalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_calendar, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        binding.calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_NONE);

        binding.calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {


                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                        .beginTransaction().replace(R.id.main_container, CalendarDetailFragment.newInstance(date))
                        .addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

        binding.calendarView.clearSelection();

        //計畫的圓點
        List<RecordModel> recordList = RealmManager.INSTANCE.retrieveAllRecords();

        for (RecordModel recordModel : recordList) {
            records.add(CalendarDay.from(recordModel.getDate()));
        }

        DotDecorator dotDecorator = new DotDecorator(getResources().getColor(R.color.transYellow), records);
        binding.calendarView.addDecorators(dotDecorator);


        //規劃的線

        List<PlanModel> planModelList = RealmManager.INSTANCE.retrieveAllPlans();

        for (PlanModel planModel : planModelList) {
            plans.add(new CalendarDay(planModel.getDate()));
        }

        LineDecorator lineDecorator = new LineDecorator(getResources().getColor(R.color.colorPrimary), plans);

        binding.calendarView.addDecorators(lineDecorator);
    }
}
