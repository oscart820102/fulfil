package com.example.art.fulfil.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.CustomPickerBinding;

/**
 * Created by art on 08/02/2017.
 */

public class CustomPickerView extends RelativeLayout {
    private CustomPickerBinding binding;
    private Context context;
    private String hint = "";
    private Integer textAlignment;
    private int hintAlignment;
    private String text = "";

    public CustomPickerView(Context context) {
        super(context);
        init(context, null);
    }

    public CustomPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_picker, this, true);

        this.context = context;
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomPickerView,
                0, 0);

        try {
            text = a.getString(R.styleable.CustomPickerView_defaultText);
            hint = a.getString(R.styleable.CustomPickerView_hint);
            textAlignment = a.getInteger(R.styleable.CustomPickerView_alignment, 4);
            hintAlignment = a.getInteger(R.styleable.CustomPickerView_hintAlignment, 4);
        } finally {
            a.recycle();
        }

        if (text != null) {
            binding.customPickerText.setTextColor(getResources().getColor(R.color.darkBlackText));
            binding.customPickerText.setText(text);
        } else
            binding.customPickerText.setText(hint);

        //noinspection WrongConstant
        binding.customPickerText.setTextAlignment(hintAlignment);


    }

    public void setDialogList(final String[] list, final OnListClickListener onListClickListener) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setItems(list, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onListClickListener.onListClick(i);
                binding.customPickerText.setText(list[i]);
                binding.customPickerText.setTextColor(getResources().getColor(R.color.darkBlackText));
                binding.customPickerText.setTextAlignment(textAlignment);

            }
        });

        binding.customPickerLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.show();
            }
        });
    }


    public interface OnListClickListener {
        void onListClick(int which);
    }

    public void setText(String text) {
        binding.customPickerText.setTextColor(getResources().getColor(R.color.darkBlackText));
        binding.customPickerText.setText(text);
    }
}
