package com.example.art.fulfil.calendar;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.Constant;
import com.example.art.fulfil.R;
import com.example.art.fulfil.Util.Date2Day;
import com.example.art.fulfil.databinding.FragmentCalendarDetailBinding;
import com.example.art.fulfil.manager.RealmManager;
import com.example.art.fulfil.model.ImageDescriptionModel;
import com.example.art.fulfil.model.PlanModel;
import com.example.art.fulfil.model.RealmString;
import com.example.art.fulfil.model.RecordModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmList;

public class CalendarDetailFragment extends Fragment {
    private FragmentCalendarDetailBinding binding;
    private CalendarDay date;
    private RecordModel record;
    private PlanModel plan;
    private NoteAdapter noteAdapter;

    public CalendarDetailFragment() {
        // Required empty public constructor
    }

    public static CalendarDetailFragment newInstance(CalendarDay date) {
        CalendarDetailFragment fragment = new CalendarDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constant.FRAGMENT_CALENDAR, date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            date = getArguments().getParcelable(Constant.FRAGMENT_CALENDAR);
            record = RealmManager.INSTANCE.retrieveRecord(date.getDate());
            plan = RealmManager.INSTANCE.retrievePlan(date.getDate());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calendar_detail, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        RealmList<ImageDescriptionModel> imageDescriptionModels = (record == null) ? new RealmList<ImageDescriptionModel>() : record.getRecordList();
        RealmList<RealmString> descriptionModels = (plan == null) ? new RealmList<RealmString>() : plan.getDescriptionList();

        noteAdapter = new NoteAdapter(imageDescriptionModels, descriptionModels);
        binding.recyclerView.setAdapter(noteAdapter);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        binding.fragmentCalendarDetailDateView.setDate(date.getYear(), date.getMonth() + 1, date.getDay(), Date2Day.getDay(date.getCalendar()));

        binding.fragmentCalendarDetailRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                        .beginTransaction().replace(R.id.main_container, CalendarAddRecordFragment.newInstance(date))
                        .addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        binding.fragmentCalendarDetailPlanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                        .beginTransaction().replace(R.id.main_container, CalendarAddPlansFragment.newInstance(date))
                        .addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    }

}
