package com.example.art.fulfil.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import io.realm.RealmObject;

/**
 * Created by art on 18/05/2017.
 */

public class ImageDescriptionModel extends RealmObject {
    private String description;
    private byte[] imageByte;

    public ImageDescriptionModel(String description, byte[] imageByte) {
        this.description = description;
        this.imageByte = imageByte;
    }

    public ImageDescriptionModel() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImageByte() {
        return imageByte;
    }

    public Bitmap getImageBitmap() {
        if (imageByte != null) {
            return BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
        }
        return null;
    }

    public void setImageByte(byte[] imageByte) {
        this.imageByte = imageByte;
    }
}
