package com.example.art.fulfil.Util;

import java.util.Calendar;

/**
 * Created by art on 07/05/2017.
 */

public class Date2Day {

    public static String getDay(Calendar calendar){
        switch (calendar.get(Calendar.DAY_OF_WEEK)){
            case 1:
                return "日";
            case 2:
                return "一";
            case 3:
                return "二";
            case 4:
                return "三";
            case 5:
                return "四";
            case 6:
                return "五";
            case 7:
                return "六";

        }
        return "未知";
    }
}
