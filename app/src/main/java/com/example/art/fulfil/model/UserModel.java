package com.example.art.fulfil.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by art on 07/05/2017.
 */

public class UserModel extends RealmObject {
    @PrimaryKey
    private String account;
    private String name;
    private String dream;
    private int points = 0;

    public UserModel() {

    }

    public UserModel(String account) {
        this.account = account;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDream() {
        return dream;
    }

    public void setDream(String dream) {
        this.dream = dream;
    }
}
