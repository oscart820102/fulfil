package com.example.art.fulfil.calendar;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.ItemHeaderBinding;
import com.example.art.fulfil.databinding.ItemPlanBinding;
import com.example.art.fulfil.databinding.ItemRecordBinding;
import com.example.art.fulfil.model.ImageDescriptionModel;
import com.example.art.fulfil.model.PlanModel;
import com.example.art.fulfil.model.RealmString;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by art on 18/05/2017.
 */

public class NoteAdapter extends RecyclerView.Adapter {
    private RealmList<ImageDescriptionModel> imageDescriptionModels;
    private RealmList<RealmString> planList;
    private List<Object> list = new ArrayList<>();

    private enum ViewType {
        HEADER,
        PLAN,
        RECORD
    }


    public NoteAdapter(RealmList<ImageDescriptionModel> imageDescriptionModels, RealmList<RealmString> planList) {
        this.imageDescriptionModels = imageDescriptionModels;
        this.planList = planList;
        if (planList.size() != 0) {
            list.add(new String("規劃："));
            for (int i = 0; i < planList.size(); i++) {
                list.add(planList.get(i));
            }
        }


        if (imageDescriptionModels.size() != 0) {
            list.add(new String("紀錄："));
            for (int i = 0; i < imageDescriptionModels.size(); i++) {
                list.add(imageDescriptionModels.get(i));
            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        if (viewType == ViewType.HEADER.ordinal()) {
            ItemHeaderBinding binding = ItemHeaderBinding.inflate(layoutInflater, parent, false);
            return new HeaderViewHolder(binding);
        } else if (viewType == ViewType.PLAN.ordinal()) {
            ItemPlanBinding binding = ItemPlanBinding.inflate(layoutInflater, parent, false);
            return new PlanViewHolder(binding);

        } else if (viewType == ViewType.RECORD.ordinal()) {
            ItemRecordBinding binding = ItemRecordBinding.inflate(layoutInflater, parent, false);
            return new RecordViewHolder(binding);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).getBinding().titleText.setText((String) list.get(position));
        } else if (holder instanceof PlanViewHolder) {
            RealmString realmString = (RealmString) list.get(position);
            ((PlanViewHolder) holder).getBinding().description.setText(realmString.getVal());
        } else if (holder instanceof RecordViewHolder) {
            ItemRecordBinding binding = ((RecordViewHolder) holder).getBinding();
            ImageDescriptionModel imageDescriptionModel = (ImageDescriptionModel) list.get(position);
            binding.description.setText(imageDescriptionModel.getDescription());
            Bitmap bitmap = imageDescriptionModel.getImageBitmap();
            if (bitmap != null)
                binding.image.setImageBitmap(bitmap);
            else
                binding.image.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemViewType(int position) {
        Object object = list.get(position);
        if (object instanceof String)
            return ViewType.HEADER.ordinal();
        else if (object instanceof RealmString) {
            return ViewType.PLAN.ordinal();
        } else if (object instanceof ImageDescriptionModel) {
            return ViewType.RECORD.ordinal();
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecordViewHolder extends RecyclerView.ViewHolder {
        private ItemRecordBinding binding;

        public RecordViewHolder(ItemRecordBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ItemRecordBinding getBinding() {
            return binding;
        }
    }

    class PlanViewHolder extends RecyclerView.ViewHolder {
        private ItemPlanBinding binding;

        public PlanViewHolder(ItemPlanBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ItemPlanBinding getBinding() {
            return binding;
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        private ItemHeaderBinding binding;

        public HeaderViewHolder(ItemHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ItemHeaderBinding getBinding() {
            return binding;
        }
    }

}
