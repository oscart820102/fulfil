package com.example.art.fulfil.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by art on 08/05/2017.
 */

public class PlanModel extends RealmObject {
    @PrimaryKey
    private long id;
    private Date date;
    private RealmList<RealmString> descriptionList;

    public PlanModel() {
    }

    public PlanModel(Date date, RealmList<RealmString> descriptionList) {
        this.date = date;
        this.descriptionList = descriptionList;
        this.id = date.getTime();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RealmList<RealmString> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(RealmList<RealmString> descriptionList) {
        this.descriptionList = descriptionList;
    }

}
