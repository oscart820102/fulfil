package com.example.art.fulfil.find_resource;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.R;
import com.example.art.fulfil.calendar.CalendarFragment;
import com.example.art.fulfil.databinding.FragmentFindResouceBinding;


public class FindResourceFragment extends Fragment {
    private FragmentFindResouceBinding binding;

    public FindResourceFragment() {
        // Required empty public constructor
    }


    public static FindResourceFragment newInstance() {
        FindResourceFragment fragment = new FindResourceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_find_resouce, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        binding.findResourceFindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                        .beginTransaction().replace(R.id.main_container, FindResourceSelectFragment.newInstance())
                        .addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
