package com.example.art.fulfil.calendar;


import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.example.art.fulfil.Constant;
import com.example.art.fulfil.R;
import com.example.art.fulfil.Util.BitmapUtil;
import com.example.art.fulfil.Util.Date2Day;
import com.example.art.fulfil.databinding.FragmentCalendarAddPlansBinding;
import com.example.art.fulfil.databinding.FragmentCalendarAddRecordBinding;
import com.example.art.fulfil.dialogs.CustomDatePickerDialog;
import com.example.art.fulfil.manager.RealmManager;
import com.example.art.fulfil.model.ImageDescriptionModel;
import com.example.art.fulfil.model.PlanModel;
import com.example.art.fulfil.model.RealmString;
import com.example.art.fulfil.model.RecordModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.joda.time.Days;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CalendarAddPlansFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarAddPlansFragment extends Fragment {

    private FragmentCalendarAddPlansBinding binding;
    private CalendarDay date;
    private CustomDatePickerDialog firstDatePickerDialog;
    private CustomDatePickerDialog secondDatePickerDialog;


    public CalendarAddPlansFragment() {
        // Required empty public constructor
    }


    public static CalendarAddPlansFragment newInstance(CalendarDay date) {
        CalendarAddPlansFragment fragment = new CalendarAddPlansFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constant.FRAGMENT_CALENDAR, date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            date = getArguments().getParcelable(Constant.FRAGMENT_CALENDAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calendar_add_plans, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        firstDatePickerDialog = new CustomDatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                firstDatePickerDialog.setDateTime(calendar);
                binding.fragmentCalendarAddPlansFirstDate.setDate(year, monthOfYear + 1, dayOfMonth);
            }
        }, date.getCalendar());

        secondDatePickerDialog = new CustomDatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                secondDatePickerDialog.setDateTime(calendar);
                binding.fragmentCalendarAddPlansSecondDate.setDate(year, monthOfYear + 1, dayOfMonth);
            }
        }, date.getCalendar());


        binding.fragmentCalendarAddPlansFirstDate.setDate(date.getYear(), date.getMonth() + 1, date.getDay());
        binding.fragmentCalendarAddPlansSecondDate.setDate(date.getYear(), date.getMonth() + 1, date.getDay());

        binding.fragmentCalendarAddPlansFirstDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstDatePickerDialog.show();
            }
        });
        binding.fragmentCalendarAddPlansSecondDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                secondDatePickerDialog.show();
            }
        });


        binding.fragmentCalendarAddPlansConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                RecordModel recordModel = RealmManager.INSTANCE.retrieveRecord(date.getDate());
//                RealmResults<PlanModel> planModels = RealmManager.INSTANCE.retrievePlansBetween(firstDatePickerDialog.getDate().getTime(), secondDatePickerDialog.getDate().getTime());
                String description = binding.editText.getText().toString();

                Date indexDate = firstDatePickerDialog.getDate().getTime();
                Date lasteDate = secondDatePickerDialog.getDate().getTime();
                while (indexDate.before(lasteDate) || indexDate.equals(lasteDate)) {
                    PlanModel planModel = RealmManager.INSTANCE.retrievePlan(lasteDate);
                    if (planModel == null) {
                        RealmList<RealmString> realmStrings = new RealmList<RealmString>();
                        realmStrings.add(new RealmString(description));
                        planModel = new PlanModel(indexDate, realmStrings);
                        RealmManager.INSTANCE.createPlan(planModel);
                    } else {
                        planModel.getDescriptionList().add(new RealmString(description));
                        RealmManager.INSTANCE.createPlan(planModel);
                    }

                    //加一天
                    Calendar c = Calendar.getInstance();
                    c.setTime(indexDate);
                    c.add(Calendar.DATE, 1);
                    indexDate = c.getTime();
                }


                getFragmentManager().popBackStack(Constant.FRAGMENT_CALENDAR, 0);
            }
        });
    }
}
