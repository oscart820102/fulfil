package com.example.art.fulfil;

/**
 * Created by art on 07/05/2017.
 */

public class Constant {
    public static final String FRAGMENT_CALENDAR = "FRAGMENT_CALENDAR";
    public static final int REQUEST_CAMERA = 1001;
    public static final int SELECT_PHOTO = 1002;

}
