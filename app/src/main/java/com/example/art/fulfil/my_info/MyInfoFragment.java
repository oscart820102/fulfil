package com.example.art.fulfil.my_info;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.art.fulfil.BR;
import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.FragmentMyInfoBinding;
import com.example.art.fulfil.manager.RealmManager;
import com.example.art.fulfil.model.UserModel;


public class MyInfoFragment extends Fragment {

    private FragmentMyInfoBinding binding;
    private UserModel user;

    public MyInfoFragment() {
        // Required empty public constructor
    }


    public static MyInfoFragment newInstance() {
        MyInfoFragment fragment = new MyInfoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = RealmManager.INSTANCE.retrieveUser("test123");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_my_info, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.setVariable(BR.user, user);
//        binding.executePendingBindings();


        binding.fragmentMyInfoName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String newName = ((EditText) view).getText().toString();
                    user.setName(newName);

                    RealmManager.INSTANCE.createUser(user);
                }
            }
        });

        binding.fragmentMyInfoDream.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String newDream = ((EditText) view).getText().toString();
                    user.setDream(newDream);

                    RealmManager.INSTANCE.createUser(user);
                }
            }
        });
    }
}
