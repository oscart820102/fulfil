package com.example.art.fulfil.manager;


import com.example.art.fulfil.model.PlanModel;
import com.example.art.fulfil.model.RecordModel;
import com.example.art.fulfil.model.UserModel;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public enum RealmManager {
    INSTANCE;

    /* 此Manager 負責所有CRUD 工作 */
    private Realm realm = Realm.getDefaultInstance();

    public void createUser(final UserModel userModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(userModel);
            }
        });
    }

//    public List<RealmKiiUser> retrieveAllKiiUsers() {
//        RealmResults<RealmKiiUser> results = realm.where(RealmKiiUser.class)
//                .findAll();
//        return realm.copyFromRealm(results);
//    }

    public UserModel retrieveUser(final String account) {
        UserModel result = realm.where(UserModel.class).equalTo("account", account)
                .findFirst();
        return (result == null) ? null : realm.copyFromRealm(result);
    }

    public void deleteRealmKiiUser(final String account) {
        final RealmResults<UserModel> results = realm.where(UserModel.class).equalTo("account", account)
                .findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
            }
        });

    }

    public void createRecord(final RecordModel recordModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(recordModel);
            }
        });
    }

    public RecordModel retrieveRecord(final Date date) {
        RecordModel result = realm.where(RecordModel.class).equalTo("id", date.getTime())
                .findFirst();
        return (result == null) ? null : realm.copyFromRealm(result);
    }


    public List<RecordModel> retrieveAllRecords() {
        RealmResults<RecordModel> results = realm.where(RecordModel.class)
                .findAll();
        return realm.copyFromRealm(results);
    }

    public void createPlan(final PlanModel planModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(planModel);
            }
        });
    }

    public PlanModel retrievePlan(final Date date) {
        PlanModel result = realm.where(PlanModel.class).equalTo("id", date.getTime())
                .findFirst();
        return (result == null) ? null : realm.copyFromRealm(result);
    }

    public RealmResults<PlanModel> retrievePlansBetween(final Date firstDate, final Date lastDate) {
        RealmResults<PlanModel> result = realm.where(PlanModel.class).between("date", firstDate, lastDate)
                .findAll();
        return result;
    }

    public List<PlanModel> retrieveAllPlans() {
        RealmResults<PlanModel> results = realm.where(PlanModel.class)
                .findAll();
        return realm.copyFromRealm(results);
    }
}
