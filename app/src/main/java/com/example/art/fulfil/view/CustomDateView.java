package com.example.art.fulfil.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.CustomDateBinding;

/**
 * Created by art on 08/02/2017.
 */

public class CustomDateView extends RelativeLayout {
    private CustomDateBinding binding;
    private Context context;

    public CustomDateView(Context context) {
        super(context);
        init(context, null);
    }

    public CustomDateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomDateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_date, this, true);

        this.context = context;

    }


    public void setDate(int year, int month, int date, String day) {
        binding.customDateYear.setText(String.format("%d", year));
        binding.customDateMonth.setText(String.format("%2d", month));
        binding.customDateDate.setText(String.format("%2d", date));
        binding.customDateDay.setText(day);
    }

    public void setDate(int year, int month, int date) {
        binding.customDateYear.setText(String.format("%d", year));
        binding.customDateMonth.setText(String.format("%2d", month));
        binding.customDateDate.setText(String.format("%2d", date));
        binding.customDateDay.setVisibility(GONE);
        binding.left.setVisibility(GONE);
        binding.right.setVisibility(GONE);
    }
}
