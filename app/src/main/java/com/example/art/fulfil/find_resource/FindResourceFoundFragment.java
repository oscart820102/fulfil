package com.example.art.fulfil.find_resource;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.FragmentFindResourceFoundBinding;


public class FindResourceFoundFragment extends Fragment {
    private FragmentFindResourceFoundBinding binding;

    public FindResourceFoundFragment() {
        // Required empty public constructor
    }


    public static FindResourceFoundFragment newInstance() {
        FindResourceFoundFragment fragment = new FindResourceFoundFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_find_resource_found, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
