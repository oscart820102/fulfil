package com.example.art.fulfil.notify;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.FragmentNotifyBinding;


public class NotifyFragment extends Fragment {
    private FragmentNotifyBinding binding;

    public NotifyFragment() {
        // Required empty public constructor
    }


    public static NotifyFragment newInstance() {
        NotifyFragment fragment = new NotifyFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_notify, container, false);
        return binding.getRoot();
    }

}
