package com.example.art.fulfil;

import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.art.fulfil.calendar.CalendarFragment;
import com.example.art.fulfil.databinding.ActivityMainBinding;
import com.example.art.fulfil.find_resource.FindResourceFragment;
import com.example.art.fulfil.manager.RealmManager;
import com.example.art.fulfil.model.UserModel;
import com.example.art.fulfil.my_info.MyInfoFragment;
import com.example.art.fulfil.notify.NotifyFragment;

import net.danlew.android.joda.JodaTimeAndroid;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityMainBinding binding;
    private UserModel user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Realm.init(this);
        JodaTimeAndroid.init(this);

        user = RealmManager.INSTANCE.retrieveUser("test123");
        if (user == null) {
            RealmManager.INSTANCE.createUser(new UserModel("test123"));
            user = RealmManager.INSTANCE.retrieveUser("test123");
        }


        binding.mainCalendar.setOnClickListener(this);
        binding.mainNotifyButton.setOnClickListener(this);
        binding.mainFindResourceButton.setOnClickListener(this);
        binding.mainMyInfoButton.setOnClickListener(this);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.main_container, MyInfoFragment.newInstance());
        fragmentTransaction.commit();
    }

    public void tabClicked(View view) {
        int themeColor = getResources().getColor(R.color.colorPrimary);
        int transparentColor = getResources().getColor(R.color.trans);


        binding.mainMyInfoButton.setSelected(false);
        binding.mainMyInfoButton.setBackgroundColor(transparentColor);
        binding.mainFindResourceButton.setSelected(false);
        binding.mainFindResourceButton.setBackgroundColor(transparentColor);
        binding.mainCalendar.setSelected(false);
        binding.mainCalendar.setBackgroundColor(transparentColor);
        binding.mainNotifyButton.setSelected(false);
        binding.mainNotifyButton.setBackgroundColor(transparentColor);
        switch (view.getId()) {
            case R.id.main_calendar:
                binding.mainCalendar.setBackgroundColor(themeColor);
                binding.mainCalendar.setSelected(true);
                break;

            case R.id.main_find_resource_button:
                binding.mainFindResourceButton.setBackgroundColor(themeColor);
                binding.mainFindResourceButton.setSelected(true);
                break;

            case R.id.main_my_info_button:
                binding.mainMyInfoButton.setBackgroundColor(themeColor);
                binding.mainMyInfoButton.setSelected(true);
                break;

            case R.id.main_notify_button:
//                binding.mainMyInfoButton.setBackgroundColor(themeColor);
                binding.mainNotifyButton.setSelected(true);

                break;

        }

    }

    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction;
        switch (view.getId()) {
            case R.id.main_calendar:
                fragmentTransaction = getSupportFragmentManager().beginTransaction().addToBackStack(Constant.FRAGMENT_CALENDAR).replace(R.id.main_container, CalendarFragment.newInstance());
                fragmentTransaction.commit();
                tabClicked(view);
                break;

            case R.id.main_find_resource_button:
                fragmentTransaction = getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.main_container, FindResourceFragment.newInstance());
                fragmentTransaction.commit();
                tabClicked(view);
                break;

            case R.id.main_my_info_button:
                fragmentTransaction = getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.main_container, MyInfoFragment.newInstance());
                fragmentTransaction.commit();
                tabClicked(view);
                break;

            case R.id.main_notify_button:
                fragmentTransaction = getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.main_container, NotifyFragment.newInstance());
                fragmentTransaction.commit();
                tabClicked(view);
                break;

        }
    }
}
