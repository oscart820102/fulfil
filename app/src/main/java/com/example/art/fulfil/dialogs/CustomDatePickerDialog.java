package com.example.art.fulfil.dialogs;

import android.app.DatePickerDialog;
import android.content.Context;


import java.util.Calendar;

/**
 * Created by SSD_win8 on 2016/8/1.
 */


public class CustomDatePickerDialog extends DatePickerDialog {
    private Calendar date;

    public Calendar getDate() {
        return date;
    }

    public void setDateTime(Calendar date) {
        this.date = date;
    }


    public CustomDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
    }

    public CustomDatePickerDialog(Context context, OnDateSetListener callBack, Calendar dateTime) {
        this(context, callBack, dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH)); //month 從0-11 故-1
        this.date = dateTime;
    }
}
