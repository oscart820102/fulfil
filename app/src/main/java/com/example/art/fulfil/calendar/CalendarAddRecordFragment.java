package com.example.art.fulfil.calendar;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.Constant;
import com.example.art.fulfil.R;
import com.example.art.fulfil.Util.BitmapUtil;
import com.example.art.fulfil.Util.Date2Day;
import com.example.art.fulfil.databinding.FragmentCalendarAddRecordBinding;
import com.example.art.fulfil.manager.RealmManager;
import com.example.art.fulfil.model.ImageDescriptionModel;
import com.example.art.fulfil.model.RecordModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmList;

import static android.Manifest.permission.CAMERA;
import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;
import static com.example.art.fulfil.Constant.REQUEST_CAMERA;
import static com.example.art.fulfil.Constant.SELECT_PHOTO;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CalendarAddRecordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarAddRecordFragment extends Fragment {

    private FragmentCalendarAddRecordBinding binding;
    private CalendarDay date;
    private Uri photoUri;
    private Bitmap photoBitmap;


    public CalendarAddRecordFragment() {
        // Required empty public constructor
    }


    public static CalendarAddRecordFragment newInstance(CalendarDay date) {
        CalendarAddRecordFragment fragment = new CalendarAddRecordFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constant.FRAGMENT_CALENDAR, date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            date = getArguments().getParcelable(Constant.FRAGMENT_CALENDAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calendar_add_record, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        binding.fragmentCalendarAddRecordDateView.setDate(date.getYear(), date.getMonth() + 1, date.getDay(), Date2Day.getDay(date.getCalendar()));

        binding.fragmentCalendarAddRecordConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecordModel recordModel = RealmManager.INSTANCE.retrieveRecord(date.getDate());
                String description = binding.fragmentCalendarAddRecordEditText.getText().toString();

                if (recordModel == null) {
                    RealmList<ImageDescriptionModel> imageDescriptionModelList = new RealmList<>();
                    imageDescriptionModelList.add(new ImageDescriptionModel(description, BitmapUtil.bitmapToByte(photoBitmap)));
                    RealmManager.INSTANCE.createRecord(new RecordModel(date.getDate(), imageDescriptionModelList));
                } else {
                    RealmList<ImageDescriptionModel> imageDescriptionModelList = recordModel.getRecordList();
                    imageDescriptionModelList.add(new ImageDescriptionModel(description, BitmapUtil.bitmapToByte(photoBitmap)));
                    RealmManager.INSTANCE.createRecord(new RecordModel(date.getDate(), imageDescriptionModelList));
                }
                getFragmentManager().popBackStack(Constant.FRAGMENT_CALENDAR, 0);
            }
        });

        binding.takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permission = ActivityCompat.checkSelfPermission(getActivity(), CAMERA);
                if (permission != PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA}, REQUEST_CAMERA);
                else
                    openCamera();
            }
        });
        binding.uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://media/internal/images/media"));
                startActivityForResult(galleryIntent, SELECT_PHOTO);
            }
        });
    }

    public void openCamera() {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        SimpleDateFormat tmpTime = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String filename = tmpTime.format(new Date()) + ".jpg";

        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File output = new File(dir, filename);
        photoUri = Uri.fromFile(output);

        intent.putExtra(EXTRA_OUTPUT, photoUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResult) {
        if (requestCode == REQUEST_CAMERA)
            openCamera();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_CAMERA && resultCode == RESULT_OK)) {

            Bitmap photoBitmap = BitmapFactory.decodeFile(photoUri.getPath());
            this.photoBitmap = photoBitmap;

            showImage(photoBitmap);
        }
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            InputStream imageStream = null;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
            } catch (FileNotFoundException e) {
                Log.e("FileNotFound", e.getMessage());
            }
            Bitmap photoBitmap = BitmapFactory.decodeStream(imageStream);
            this.photoBitmap = photoBitmap;
            showImage(photoBitmap);
            Uri photoUri = data.getData();
            if (photoUri != null) {
            }

        }
    }

    private void showImage(Bitmap bitmap) {
        binding.image.setVisibility(View.VISIBLE);
        binding.uploadImageButton.setVisibility(View.GONE);
        binding.takePhotoButton.setVisibility(View.GONE);

        binding.image.setImageBitmap(bitmap);
    }



}

