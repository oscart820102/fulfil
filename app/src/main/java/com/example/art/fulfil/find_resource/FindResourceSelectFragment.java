package com.example.art.fulfil.find_resource;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.art.fulfil.R;
import com.example.art.fulfil.databinding.FragmentFindResourceSelectBinding;
import com.example.art.fulfil.view.CustomPickerView;


public class FindResourceSelectFragment extends Fragment {
    private FragmentFindResourceSelectBinding binding;

    public FindResourceSelectFragment() {
        // Required empty public constructor
    }


    public static FindResourceSelectFragment newInstance() {
        FindResourceSelectFragment fragment = new FindResourceSelectFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_find_resource_select, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String[] typeList = {"科技", "藝文", "旅遊", "農業", "運動", "公益"};
        String[] presentList = {"展覽", "遊行", "演講", "讀書會", "實踐"};
        String[] locationList = {"北部", "中部", "南部", "東區"};


        binding.findResourceTypePicker.setDialogList(typeList, new CustomPickerView.OnListClickListener() {
            @Override
            public void onListClick(int which) {

            }
        });

        binding.findResourcePresentWayPicker.setDialogList(presentList, new CustomPickerView.OnListClickListener() {
            @Override
            public void onListClick(int which) {

            }
        });

        binding.findResourceLocationPicker.setDialogList(locationList, new CustomPickerView.OnListClickListener() {
            @Override
            public void onListClick(int which) {

            }
        });

        binding.findResourceConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                        .beginTransaction().replace(R.id.main_container, FindResourceFoundFragment.newInstance())
                        .addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
