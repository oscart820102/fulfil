package com.example.art.fulfil.model;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by art on 08/05/2017.
 */

public class RecordModel extends RealmObject {
    @PrimaryKey
    private long id;
    private Date date;

    private RealmList<ImageDescriptionModel> recordList;

    public RecordModel() {
    }

    public RecordModel(Date date, RealmList<ImageDescriptionModel> recordList) {
        this.date = date;
        this.id = date.getTime();
        this.recordList = recordList;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RealmList<ImageDescriptionModel> getRecordList() {
        return recordList;
    }
}
