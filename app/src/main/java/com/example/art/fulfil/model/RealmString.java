package com.example.art.fulfil.model;

import io.realm.RealmObject;

/**
 * Created by art on 18/05/2017.
 */

public class RealmString extends RealmObject {
    private String val = "";

    public RealmString() {

    }

    public RealmString(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
